import React from "react";
import Select from 'react-select';

function Multi() {
    const Item = [
        {
            value: 1,
            label: "VietNamese"
        },
        {
            value: 2,
            label: "Ocean"
        },
        {
            value: 3,
            label: "Green"
        },
        {
            value: 4,
            label: "Football"
        },
        {
            value: 5,
            label: "Swimming"
        },
        {
            value: 6,
            label: "Gym"
        }
    ]

    return(
        <div>
            <Select isMulti options={Item}></Select>
        </div>
    )
}

export default Multi;